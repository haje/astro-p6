﻿namespace Assets.Scripts
{
    public enum CommandType // 과연 쓰게 될지 모르겠음. 일단 임시로...
    {
        Stop,
    }

    public abstract class AICommand
    {
        public CommandType Type
        {
            get { return CommandType.Stop; }
        }
        public PlaneScript ParentPlane;

        public float RemainingDuration;

        public abstract void Execute();
        
        protected AICommand(PlaneScript parentPlane, float duration = 0)
        {
            ParentPlane = parentPlane;
            RemainingDuration = duration;
        }
    }
}
