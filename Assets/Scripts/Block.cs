﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts;
using Assets.Scripts.Blocks;
using System.Reflection;

public class Block : MonoBehaviour
{
    public bool Clicked;
    public Sprite Image;
    public Slot slot;

    //Animator animator;

    public Block[] blocks;
    public AIBlock aiBlock;
    public string blockName;
    public GameObject Bullet;
    public bool Targetted;
    public bool Close;

    // Use this for initialization
    void Start()
    {
        //animator = GetComponent<Animator>();
        Clicked = false;
        Image = GetComponent<Image>().sprite;
        try
        {
            blocks = slot.blocks;
        }
        catch { }



    }

    public void GetClicked()
    {

        if (Bullet)
        {
            aiBlock = new Shoot(null, Bullet, Close, Targetted);
        }
        else
        {
            //Debug.Log("HELLO");
            if (blockName == "") return;
            AIBlock oldblock = aiBlock;
            aiBlock = (AIBlock)System.Activator.CreateInstance(null, "Assets.Scripts.Blocks." + blockName).Unwrap();
            Debug.Assert(aiBlock != oldblock);
        }

        foreach (Block block in blocks)
        {
            if (block == this)
            {

            }
            else
            {
                if (block.Clicked)
                {
                    block.Clicked = false;
                }
            }

        }

        if (!(Clicked))
        {
            Clicked = true;
        }
        else
        {
            Clicked = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
