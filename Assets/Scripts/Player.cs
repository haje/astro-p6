﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class Player
    {
        public List<PlaneScript> Planes = new List<PlaneScript>();
        public Player Opponent;
    }
}