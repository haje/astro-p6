﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts;
using Assets.Scripts.Blocks;

public class Slot : MonoBehaviour {

    public ConfirmButton confirmButton;

    public bool activated;
    public GameObject blockbox;
    private Image SlotImage;
    private Button SlotButton;
    private Text SlotText;
    Animator animator;

    public Slot[] Slots;
    public Block[] blocks;

    public AIBlock aiBlock;

    public Sprite defaultSprite;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        activated= false;
        aiBlock = new NoPreferredPosition(null);
        SlotImage = GetComponent<Image>();
        SlotButton = GetComponent<Button>();
        SlotText = SlotButton.GetComponentInChildren<Text>();
    }

    public void GetClicked()
    {
        foreach (Slot slot in Slots)
        {
            if (slot.activated)
            {
                foreach (Block block in blocks)
                {
                    if (block.Clicked)
                    {
                        block.Clicked = false;
                    }
                }
                slot.activated = false;
                slot.blockbox.SetActive(false);
                slot.animator.SetBool("Activated", false);
            }
        }
        if(!(activated))
        {
            activated = true;
            blockbox.SetActive(true);
            animator.SetBool("Activated", true);
        }
        else
        {
            activated = false;
            blockbox.SetActive(false);
            animator.SetBool("Activated", false);
        }

    }
	
	// Update is called once per frame
	void Update () {

        if(activated)
        {
            foreach (Block block in blocks)
            {
                if (block.Clicked)
                {
                    SlotImage = GetComponent<Image>();
                    SlotImage.sprite = block.Image;
                    aiBlock = block.aiBlock;
                    GameObject.Find("SelectedSlot").GetComponent<Text>().text = block.Bullet == null ? block.blockName : block.Bullet.name;
                    SlotButton = GetComponent<Button>();
                    SlotText = SlotButton.GetComponentInChildren<Text>();
                    SlotText.text = null;
                    block.Clicked = false;

                    activated = false;
                    blockbox.SetActive(false);
                    animator.SetBool("Activated", false);
                }
            }
        }
        

        if (confirmButton.confirmed)
        {
            SlotImage.sprite = defaultSprite;
            //aiBlock = new NoPreferredPosition(null);
            GameObject.Find("SelectedSlot").GetComponent<Text>().text = "";
            SlotText.text = "Click Here";
            activated = false;
            blockbox.SetActive(false);
            animator.SetBool("Activated", false);
        }
	}
}
