﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Blocks;

public class ConfirmButton : MonoBehaviour
{

    public float Velocity;
    public bool confirmed;

    public Camera CurrentCamera;
    public Camera NextCamera;

    public GameObject CurrentPlane;
    public GameObject NextPlane;

    public GameObject[] Planes;
    private int nth;

    public List<Slot> Slots;

    // Use this for initialization
    void Start()
    {
        confirmed = false;
        nth = 0;
        CurrentPlane = Planes[nth];
        NextPlane = Planes[(nth + 1)];
        NextCamera.enabled = false;

        /*
        Slots[0].aiBlock = new NoPreferredPosition();
        Slots[1].aiBlock = new TargetClosest();
        Slots[2].aiBlock = new NoPreferredPosition();
        Slots[3].aiBlock = new NoPreferredPosition();
        */
    }

    public void GetClicked()
    {
        confirmed = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (confirmed)
        {
            

            /*
            Slots[0].aiBlock = new NoPreferredPosition();
            Slots[1].aiBlock = new TargetClosest();
            Slots[2].aiBlock = new NoPreferredPosition();
            Slots[3].aiBlock = new NoPreferredPosition();
            */
            if (nth == 2)
            {
                Assets.Scripts.WorldManager.blocks.Add(new List<Assets.Scripts.AIBlock>()
                    {Slots[0].aiBlock, Slots[1].aiBlock, Slots[2].aiBlock, Slots[3].aiBlock });

                //confirmed = false;
                SceneManager.LoadScene("Test Scene");
            }
            else if (CurrentCamera.transform.position.x > (NextPlane.transform.position.x - 1.5f))
            {

                

                Assets.Scripts.WorldManager.blocks.Add(new List<Assets.Scripts.AIBlock>()
                    {Slots[0].aiBlock, Slots[1].aiBlock, Slots[2].aiBlock, Slots[3].aiBlock });


                CurrentCamera.transform.position = new Vector3((NextPlane.transform.position.x - 1.5f), CurrentCamera.transform.position.y, CurrentCamera.transform.position.z);
                if (nth == 0)
                {
                    nth = nth + 1;
                    CurrentPlane = Planes[nth];
                    NextPlane = Planes[(nth + 1)];
                }
                else if (nth == 1)
                {
                    nth = nth + 1;
                    CurrentPlane = Planes[nth];
                    NextPlane = null;
                }



                confirmed = false;
            }
            CurrentCamera.transform.position = CurrentCamera.transform.position + new Vector3(Velocity, 0, 0);

        }
    }
}
