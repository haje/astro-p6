﻿using UnityEngine;

namespace Assets.Scripts
{
    class BulletScript : MonoBehaviour
    {
        public PlaneScript Target;
        public GameObject ExplosionEffect;
        public float speed;
        public float MaxAngularSpeed;
        public Player Owner;
        public float cooldown;
        public bool IsBomb;

        void Update()
        {
            if (Target)
            {
                if (Target.Dead) Target = null;
                //transform.LookAt(Target.transform);

                Vector3 front = transform.forward;
                Vector3 dest = Target.transform.position - transform.position;

                float d = Vector3.Cross(front, dest).y / front.magnitude / dest.magnitude;

                float angle = (Vector3.Angle(front, dest));
                if (angle == 180) d = 1;

                float angSpeed = (angle >= 90 ? (Mathf.Sign(d)) : d) * MaxAngularSpeed;

                Vector3 eRo = transform.localRotation.eulerAngles;
                transform.localRotation = Quaternion.Euler(new Vector3(eRo.x, eRo.y + Time.deltaTime * angSpeed
                    , eRo.z + 200f * Time.deltaTime));
            }
            transform.Translate(0, 0, speed * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);

            if (Mathf.Max(Mathf.Abs(transform.position.x), Mathf.Abs(transform.position.z)) > 50) Destroy(gameObject);
        }

        public void Explode()
        {
            if(ExplosionEffect) Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        public int Damage;

        void OnTriggerEnter(Collider other)
        {
            BulletScript bullet;
            if ((bullet = other.GetComponent<BulletScript>()) && bullet.Owner != Owner && (IsBomb || bullet.IsBomb))
            {
                bullet.Explode();
                Explode();
            }
        }
    }
}
