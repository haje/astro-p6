﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Commands;
using Assets.Scripts.Blocks;

namespace Assets.Scripts
{
    public enum PlaneTurnDirections
    {
        Left, Right, Front
    }

    public class PlaneScript : MonoBehaviour
    {
        public List<AIBlock> AIBlocks = new List<AIBlock>();
        public List<AICommand> Commands = new List<AICommand>();

        public GameObject ExplosionEffect;

        public Player Owner;

        public float Cooldown;


        [SerializeField]
        private int hp = 100;
        public int HP
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
                if (hp <= 0) Die();
            }
        }

        public int MaxHP
        {
            get; set;
        }

        public void ReceiveCommand(AICommand command)
        {
            Commands.Add(command);
        }

        /*
        public Quaternion Rotation
        {
            get
            {
                return transform.rotation;
            }
            set
            {
                transform.rotation = value;
            }
        }
        */
        public float MaxSpeed;
        public float MaxAccel;
        public float MaxAngularSpeed;

        public float Acceleration;
        public float Speed;
        public float AngularSpeed;
        public PlaneTurnDirections Direction = PlaneTurnDirections.Front;

        public PlaneScript Target;

        public bool closeToTarget;

        public bool Dead = false;

        public const float MaxTiltDegree = 30;
        public float TiltDegree;

        void Update()
        {
            /*
            ReceiveCommand(new Rotate(this));
            ReceiveCommand(new MoveFullSpeed(this));

            */

            Cooldown -= Time.deltaTime;
            if (Cooldown < 0) Cooldown = 0;

            Speed += Acceleration * Time.deltaTime;
            Speed = Mathf.Clamp(Speed, 0, MaxSpeed);
            if (Speed < MaxSpeed) Speed = MaxSpeed;
            transform.Translate(0, 0, Speed * Time.deltaTime);

            TiltDegree -= Mathf.Sign(TiltDegree) * Time.deltaTime * 0.1f;

            List<AICommand> commandgrave = new List<AICommand>();
            foreach (AICommand command in Commands)
            {
                command.Execute();
                command.RemainingDuration -= Time.deltaTime;
                if (command.RemainingDuration <= 0)
                    commandgrave.Add(command);
            }

            foreach (AICommand command in commandgrave)
                Commands.Remove(command);


            foreach (AIBlock block in AIBlocks)
            {
                block.Update();
            }

            //if (!Target) AngularSpeed = 0;

            if (Mathf.Max(Mathf.Abs(transform.position.x), Mathf.Abs(transform.position.z)) > 25)
            {
                ReceiveCommand(new Rotate(this, Vector3.zero));
            }

            if (Direction != PlaneTurnDirections.Front)
            {
                int dirFactor = (Direction == PlaneTurnDirections.Right ? 1 : -1);

                //Debug.Log(TiltDegree + AngularSpeed * -dirFactor * Time.deltaTime);
                TiltDegree = Mathf.Clamp(TiltDegree + AngularSpeed * -dirFactor * Time.deltaTime * 0.4f, -MaxTiltDegree, MaxTiltDegree);

                Vector3 eRo = transform.localRotation.eulerAngles;
                transform.localRotation = Quaternion.Euler(new Vector3(eRo.x, eRo.y + Time.deltaTime * AngularSpeed * dirFactor
                    , TiltDegree));
            }


            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }

        void OnTriggerEnter(Collider other)
        {
            BulletScript bullet;
            if ((bullet = other.GetComponent<BulletScript>()) && bullet.Owner != Owner)
            {
                HP -= bullet.Damage;
                bullet.Explode();
            }
        }

        void Die()
        {
            Dead = true;
            Instantiate(ExplosionEffect, transform.position, Quaternion.identity);

            foreach (Player player in WorldManager.Players)
                foreach (PlaneScript plane in player.Planes)
                    if (plane.Target == this) plane.Target = null;

            Owner.Planes.Remove(this);

            if (Owner.Planes.Count == 0)
                if (Owner == WorldManager.Player1)
                {
                    WorldManager.Instance.GameOver.SetActive(true);
                    Time.timeScale = 0.01f;
                }
                else
                {
                    WorldManager.Instance.YouWin.SetActive(true);
                    Time.timeScale = 0.01f;
                }

            Destroy(gameObject);
        }

    }
}
