﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Blocks;

namespace Assets.Scripts
{
    public class WorldManager : MonoBehaviour
    {
        private static WorldManager instance;
        public static WorldManager Instance { get { return instance; } }

        public static Player Player1 = new Player();
        public static Player Player2 = new Player();
        public static Player[] Players = new Player[] { Player1, Player2 };
        public GameObject[] Bullets;

        public static List<List<AIBlock>> blocks = new List<List<AIBlock>>();

        public List<Camera> Cameras = new List<Camera>();
        public Camera Cam;
        public Camera MainCamera;

        public GameObject SamplePlane;

        public Material EnemyMaterial;

        public GameObject YouWin;
        public GameObject GameOver;

        void Awake() { instance = this; }

        void Start()
        {
            Player1.Opponent = Player2;
            Player2.Opponent = Player1;

            Quaternion r = Quaternion.Euler(0, 180, 0);

            Player1.Planes.Add(((GameObject)Instantiate(SamplePlane, new Vector3(5, 0, 20), r)).GetComponent<PlaneScript>());
            Player1.Planes.Add(((GameObject)Instantiate(SamplePlane, new Vector3(0, 0, 20), r)).GetComponent<PlaneScript>());
            Player1.Planes.Add(((GameObject)Instantiate(SamplePlane, new Vector3(-5, 0, 20), r)).GetComponent<PlaneScript>());

            Player2.Planes.Add(((GameObject)Instantiate(SamplePlane, new Vector3(5, 0, -20), Quaternion.identity)).GetComponent<PlaneScript>());
            Player2.Planes.Add(((GameObject)Instantiate(SamplePlane, new Vector3(0, 0, -20), Quaternion.identity)).GetComponent<PlaneScript>());
            Player2.Planes.Add(((GameObject)Instantiate(SamplePlane, new Vector3(-5, 0, -20), Quaternion.identity)).GetComponent<PlaneScript>());

            /*
            foreach (PlaneScript plane in Player1.Planes)
            {
                plane.Owner = Player1;
            }*/
            
            for (int i = 0; i < 3; i++)
            {
                Player1.Planes[i].Owner = Player1;
                Player1.Planes[i].AIBlocks = new List<AIBlock>(blocks[i]);



                /*
                Player1.Planes[i].AIBlocks.Add(new PreferCloseCombat(Player1.Planes[i]));
                Player1.Planes[i].AIBlocks.Add(new TargetClosest(Player1.Planes[i]));
                Player1.Planes[i].AIBlocks.Add(new Shoot(Player1.Planes[i], Bullets[0], true, false));
                Player1.Planes[i].AIBlocks.Add(new Shoot(Player1.Planes[i], Bullets[1], false, true));
                */
                
                foreach (AIBlock block in Player1.Planes[i].AIBlocks)
                {
                    block.ParentPlane = Player1.Planes[i];
                    Debug.Log(block.GetType().Name);
                }
                Debug.Log("");
                
            }

            Debug.Assert(Player1.Planes[0].AIBlocks[0].ParentPlane == Player1.Planes[0]);

            foreach (PlaneScript plane in Player2.Planes)
            {
                plane.Owner = Player2;
                plane.GetComponentInChildren<MeshRenderer>().material = EnemyMaterial;

            }


            Player2.Planes[0].AIBlocks.Add(new PreferCloseCombat(Player2.Planes[0]));
            Player2.Planes[0].AIBlocks.Add(new TargetClosest(Player2.Planes[0]));
            Player2.Planes[0].AIBlocks.Add(new Shoot(Player2.Planes[0], Bullets[0], true, false));
            Player2.Planes[0].AIBlocks.Add(new Shoot(Player2.Planes[0], Bullets[2], false, true));

            Player2.Planes[1].AIBlocks.Add(new PreferRangedCombat(Player2.Planes[1]));
            Player2.Planes[1].AIBlocks.Add(new TargetFarthestPlane(Player2.Planes[1]));
            Player2.Planes[1].AIBlocks.Add(new Shoot(Player2.Planes[1], Bullets[2], true, true));
            Player2.Planes[1].AIBlocks.Add(new Shoot(Player2.Planes[1], Bullets[2], false, true));


            Player2.Planes[2].AIBlocks.Add(new PreferRangedCombat(Player2.Planes[2]));
            Player2.Planes[2].AIBlocks.Add(new TargetCaptainPlane(Player2.Planes[2]));
            Player2.Planes[2].AIBlocks.Add(new Shoot(Player2.Planes[2], Bullets[1], true, false));
            Player2.Planes[2].AIBlocks.Add(new Shoot(Player2.Planes[2], Bullets[0], false, false));

            foreach (Player player in Players)
                foreach (PlaneScript plane in player.Planes)
                {
                    
                }

            foreach (PlaneScript plane in Player1.Planes)
            {
                Cameras.Add((Camera)Instantiate(Cam, plane.transform.position + new Vector3 (0f,0.2f,-0.2f), plane.transform.rotation,plane.transform));
            }

            foreach (Camera camera in Cameras)
            {
                camera.enabled = false;
                camera.GetComponent<AudioListener>().enabled = false;
                camera.transform.position = camera.transform.position + new Vector3(0f, 0.05f, 1f);
            }

            MainCamera.enabled = true;
        }

        private void Update()
        {



            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Cameras[0].enabled = true;
                Cameras[0].GetComponent<AudioListener>().enabled = true;
                foreach (Camera camera in Cameras)
                {
                    if(camera != Cameras[0])
                    {
                        camera.enabled = false;
                        camera.GetComponent<AudioListener>().enabled = false;
                    }
                }
                MainCamera.enabled = false;
                MainCamera.GetComponent<AudioListener>().enabled = false;
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Cameras[1].enabled = true;
                Cameras[1].GetComponent<AudioListener>().enabled = true;
                foreach (Camera camera in Cameras)
                {
                    if (camera != Cameras[1])
                    {
                        camera.enabled = false;
                        camera.GetComponent<AudioListener>().enabled = false;
                    }
                }
                MainCamera.enabled = false;
                MainCamera.GetComponent<AudioListener>().enabled = false;
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Cameras[2].enabled = true;
                Cameras[2].GetComponent<AudioListener>().enabled = true;
                foreach (Camera camera in Cameras)
                {
                    if (camera != Cameras[2])
                    {
                        camera.enabled = false;
                        camera.GetComponent<AudioListener>().enabled = false;
                    }
                }
                MainCamera.enabled = false;
                MainCamera.GetComponent<AudioListener>().enabled = false;
            }

            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                MainCamera.enabled = true;
                foreach (Camera camera in Cameras)
                {
                    if (camera != null)
                    {
                        camera.enabled = false;
                        camera.GetComponent<AudioListener>().enabled = false;
                    }
                }
                MainCamera.GetComponent<AudioListener>().enabled = true;
            }

            if(MainCamera.enabled)
            {
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    MainCamera.transform.position = MainCamera.transform.position + new Vector3(-0.1f, 0f, -0.1f);
                }
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    MainCamera.transform.position = MainCamera.transform.position + new Vector3(0.1f, 0f, 0.1f);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    MainCamera.transform.position = MainCamera.transform.position + new Vector3(-0.1f, 0f, 0.1f);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    MainCamera.transform.position = MainCamera.transform.position + new Vector3(0.1f, 0f, -0.1f);
                }
                if (Input.GetAxis("Mouse ScrollWheel")>0f)
                {
                    MainCamera.transform.position = MainCamera.transform.position + new Vector3(0f, -0.1f, 0f);
                }
                if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                {
                    MainCamera.transform.position = MainCamera.transform.position + new Vector3(0f, 0.1f, 0f);
                }
            }
            
        }

    }
}

