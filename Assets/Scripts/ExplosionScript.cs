﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class ExplosionScript : MonoBehaviour
    {
        ParticleSystem pSystem;
        // Use this for initialization
        void Start()
        {
            pSystem = GetComponentInChildren<ParticleSystem>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!pSystem.IsAlive()) Destroy(gameObject);
        }
    }
}