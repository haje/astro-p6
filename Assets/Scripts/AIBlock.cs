﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public abstract class AIBlock
    {
        public PlaneScript ParentPlane = null;

        public void SendCommand(AICommand command)
        {
            ParentPlane.ReceiveCommand(command);
        }

        public abstract void Update();

        protected AIBlock(PlaneScript parentPlane)
        {
            ParentPlane = parentPlane;
        }

        public AIBlock() { }
    }
}