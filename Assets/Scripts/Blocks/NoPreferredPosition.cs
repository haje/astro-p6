﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Commands;

namespace Assets.Scripts.Blocks
{
    class NoPreferredPosition : AIBlock
    {
        public NoPreferredPosition(PlaneScript parentPlane) : base(parentPlane)
        {
        }

        public NoPreferredPosition() { }

        public override void Update()
        {
            SendCommand(new MoveFullSpeed(ParentPlane));

            if (ParentPlane.Target != null &&
                Vector3.Distance(ParentPlane.Target.transform.position, ParentPlane.transform.position) > 18)
            {
                SendCommand(new Rotate(ParentPlane, ParentPlane.Target.transform.position));
            }
            else
            {
                SendCommand(new Rotate(ParentPlane, PlaneTurnDirections.Front, 0));
            }
        }
    }
}
