﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Commands;

namespace Assets.Scripts.Blocks
{
    class PreferRangedCombat : AIBlock
    {
        public PreferRangedCombat(PlaneScript parentPlane) : base(parentPlane)
        {

        }

        public PreferRangedCombat() { }

        private Vector3 OppositePoint;

        public override void Update()
        {
            SendCommand(new MoveFullSpeed(ParentPlane));

            if (!ParentPlane.Target) return;

            OppositePoint = 2 * ParentPlane.transform.position - ParentPlane.Target.transform.position;
            
            if (ParentPlane.Target != null && Vector3.Distance(ParentPlane.transform.position, ParentPlane.Target.transform.position) < 10)
            {
                    SendCommand(new Rotate(ParentPlane, OppositePoint));
            }

            else if (ParentPlane.Target != null && Vector3.Distance(ParentPlane.transform.position, ParentPlane.Target.transform.position) > 20)
            {
                SendCommand(new Rotate(ParentPlane, ParentPlane.Target.transform.position));
            }

            else
            {
                SendCommand(new Rotate(ParentPlane, PlaneTurnDirections.Front, 0));
            }


        }
    }

}