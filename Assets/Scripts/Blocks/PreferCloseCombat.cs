﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Commands;

namespace Assets.Scripts.Blocks
{
    class PreferCloseCombat : AIBlock
    {
        public PreferCloseCombat(PlaneScript parentPlane) : base(parentPlane)
        {
        }

        public PreferCloseCombat() { }

        public override void Update()
        {
            

            if (!ParentPlane.Target) return;
            SendCommand(new Rotate(ParentPlane, ParentPlane.Target.transform.position));
            SendCommand(new MoveFullSpeed(ParentPlane));
        }
        
    }
}
