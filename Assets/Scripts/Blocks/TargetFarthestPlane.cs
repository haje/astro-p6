﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Blocks
{
    class TargetFarthestPlane : AIBlock
    {
        public TargetFarthestPlane(PlaneScript parentPlane) : base(parentPlane)
        {

        }

        public TargetFarthestPlane() { }

        public override void Update()
        {

            float maxdist = 0f;
            PlaneScript farthestenemyplane = null;
            Vector3 CenterPoint;
            Vector3 PositionSum = new Vector3 (0,0,0);

            float P1PlanesNo = ParentPlane.Owner.Planes.Count;
            float P2PlanesNo = ParentPlane.Owner.Opponent.Planes.Count;

            for(int i = 0; i<P1PlanesNo ; i++)
            {
                PositionSum = PositionSum + ParentPlane.Owner.Planes[i].transform.position;
            }

            for (int i = 0; i < P2PlanesNo; i++)
            {
                PositionSum = PositionSum + ParentPlane.Owner.Opponent.Planes[i].transform.position;
            }

            CenterPoint = PositionSum / (P1PlanesNo + P2PlanesNo);

            foreach (PlaneScript plane in ParentPlane.Owner.Opponent.Planes)
            {
                if (maxdist < Vector3.Distance(CenterPoint, plane.transform.position))
                {
                    maxdist = Vector3.Distance(CenterPoint, plane.transform.position);
                    farthestenemyplane = plane;
                }

            }
            ParentPlane.Target = farthestenemyplane;
        }
    }
}
