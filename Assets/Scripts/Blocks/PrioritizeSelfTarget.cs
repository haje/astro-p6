﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Blocks
{
    class PrioritizeSelfTarget : AIBlock
    {
        public PrioritizeSelfTarget(PlaneScript parentPlane) : base(parentPlane)
        {

        }

        public PrioritizeSelfTarget() { }

        public override void Update()
        {

            float mindist = float.PositiveInfinity;
            PlaneScript closestenemyplane = null;
            foreach (PlaneScript plane in ParentPlane.Owner.Opponent.Planes)
            {
                if (mindist > Vector3.Distance(ParentPlane.transform.position, plane.transform.position))
                {
                    mindist = Vector3.Distance(ParentPlane.transform.position, plane.transform.position);
                    closestenemyplane = plane;
                }

            }
            if (ParentPlane.Target == null || (mindist * 2 > Vector3.Distance(ParentPlane.transform.position, ParentPlane.Target.transform.position)))
            {
                ParentPlane.Target = closestenemyplane;
            }
        }
    }
}
