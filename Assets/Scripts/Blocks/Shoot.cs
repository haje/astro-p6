﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Commands;

namespace Assets.Scripts.Blocks
{
    class Shoot : AIBlock
    {
        public bool Close;
        public bool Targetted;
        public GameObject Bullet;
        public Shoot(PlaneScript parentPlane, GameObject bullet, bool close, bool targetted) : base(parentPlane)
        {
            Bullet = bullet;
            Targetted = targetted;
            Close = close;
        }

        public override void Update()
        {
            if (ParentPlane.Target && (ParentPlane.Cooldown == 0) &&
                (Close == Vector3.Distance(ParentPlane.transform.position, ParentPlane.Target.transform.position) < 20))
            {
                if (Targetted)
                    SendCommand(new Launch(ParentPlane.Target, Bullet, ParentPlane));

                

                else if (Vector3.Angle(ParentPlane.transform.forward, ParentPlane.Target.transform.position - ParentPlane.transform.position) < 15)
                {
                    SendCommand(new Launch(ParentPlane.transform.forward, Bullet, ParentPlane));
                }
            }
        }
    }
}
