﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Blocks
{
    class TargetCaptainPlane : AIBlock
    {
        public TargetCaptainPlane(PlaneScript parentPlane) : base(parentPlane)
        {

        }

        public TargetCaptainPlane() { }

        public override void Update()
        {
            if (ParentPlane.Owner.Opponent.Planes[0] != null)
            {
                ParentPlane.Target = ParentPlane.Owner.Opponent.Planes[0];
            }
            else //if EnemyCaptainPlane is destroyed, Target AI is set to default.
            {
                float mindist = float.PositiveInfinity;
                PlaneScript closestenemyplane = null;
                foreach (PlaneScript plane in ParentPlane.Owner.Opponent.Planes)
                {
                    if (mindist > Vector3.Distance(ParentPlane.transform.position, plane.transform.position))
                    {
                        mindist = Vector3.Distance(ParentPlane.transform.position, plane.transform.position);
                        closestenemyplane = plane;
                    }

                }
                if (ParentPlane.Target == null || (mindist * 2 > Vector3.Distance(ParentPlane.transform.position, ParentPlane.Target.transform.position)))
                {
                    ParentPlane.Target = closestenemyplane;
                }
            }
        }
    }
}
