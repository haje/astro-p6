﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Commands
{
    class MoveFullSpeed : AICommand
    {
        public MoveFullSpeed(PlaneScript parentPlane, float duration = 0) : base(parentPlane, duration)
        {
        }

        public override void Execute()
        {
            ParentPlane.Acceleration = ParentPlane.MaxAccel;
        }
    }
}
