﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Commands
{
    class Launch : AICommand
    {
        public GameObject Bullet;
        PlaneScript targetPlane = null;
        Quaternion InitialRotation;

        private Launch(PlaneScript parentPlane, float duration = 0) : base(parentPlane, duration)
        {
        }

        public Launch(PlaneScript Target, GameObject Bullet, PlaneScript parentPlane, float duration = 0) : this(parentPlane, duration)
        {
            targetPlane = Target;
            this.Bullet = Bullet;
        }

        public Launch(Vector3 InitialDirection, GameObject Bullet, PlaneScript parentPlane, float duration = 0) : this(parentPlane, duration)
        {
            this.Bullet = Bullet;
            InitialRotation = Quaternion.LookRotation(InitialDirection);
        }

        public override void Execute()
        {
            BulletScript bullet = ((GameObject)GameObject.Instantiate(Bullet, ParentPlane.transform.position, ParentPlane.transform.rotation)).GetComponent<BulletScript>();
            if (targetPlane) bullet.Target = targetPlane;
            else bullet.transform.rotation = InitialRotation;
            bullet.Owner = ParentPlane.Owner;
            ParentPlane.Cooldown += bullet.cooldown;
        }
    }
}
