﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Commands
{
    class Rotate : AICommand
    {
        GameObject Bullet;

        public Rotate(PlaneScript parentPlane = null, float duration = 0) : base(parentPlane, duration)
        {

        }

        public Rotate(PlaneScript parentPlane, PlaneTurnDirections direction, float angularSpeed, float duration = 0)
            : this(parentPlane, duration)
        {
            this.direction = direction;
            angSpeed = angularSpeed;
        }

        public Rotate(PlaneScript parentPlane, Vector3 placeToLookAt, float duration = 0) : this(parentPlane, duration)
        {
            Vector3 front = ParentPlane.transform.forward;
            Vector3 dest = placeToLookAt - ParentPlane.transform.position;
            
            float d = -Vector3.Cross(front, dest).y / front.magnitude / dest.magnitude;

            if (d == 0) direction = PlaneTurnDirections.Front;
            else if (d > 0) direction = PlaneTurnDirections.Left;
            else direction = PlaneTurnDirections.Right;

            angSpeed = (Vector3.Angle(front, dest) >= 90 ? 1 : d) * ParentPlane.MaxAngularSpeed;


        }

        PlaneTurnDirections direction;
        float angSpeed;

        public override void Execute()
        {
            //Debug.Log(angSpeed);
            ParentPlane.AngularSpeed = Mathf.Clamp(angSpeed, 0, ParentPlane.MaxAngularSpeed);
            ParentPlane.Direction = direction;

        }
    }
}
