﻿using UnityEngine;
using System.Collections;

public class TESTCameraScript : MonoBehaviour {

    public Transform LookAtTarget;
	
	// Update is called once per frame
	void Update () {
        if (LookAtTarget)
        {
            transform.LookAt(LookAtTarget);
            /*
            if (Vector3.Distance(transform.position, LookAtTarget.position) >= 6)
                transform.Translate(transform.forward * 4 * Time.deltaTime);*/
        }

	}
}
